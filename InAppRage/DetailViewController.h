//
//  DetailViewController.h
//  InAppRage
//
//  Created by Nishant Thite on 08/09/15.
//  Copyright (c) 2015 Nishant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

