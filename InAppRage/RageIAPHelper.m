//
//  RageIAPHelper.m
//  InAppRage
//
//  Created by Nishant Thite on 08/09/15.
//  Copyright (c) 2015 Nishant. All rights reserved.
//

#import "RageIAPHelper.h"

@implementation RageIAPHelper

+ (RageIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static RageIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.creodev.nishant.book",
                                      @"com.creodev.nishant.pen",
                                      @"com.creodev.nishant.rubber",
                                      @"com.creodev.nishant.board",
                                      @"com.creodev.nishant.notepad",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}


@end
