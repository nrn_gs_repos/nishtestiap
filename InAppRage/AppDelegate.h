//
//  AppDelegate.h
//  InAppRage
//
//  Created by Nishant Thite on 08/09/15.
//  Copyright (c) 2015 Nishant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

